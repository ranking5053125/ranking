class Student:
    def __init__(self, name: str, subjects: list[int]):
        self.name = name
        self.marks = subjects  #

class Result:
    def __init__(self, students: list[list]):
        self.students = students  
        self.subject_count = len(students[0][1])  
       

    def compare_and_rank(self):
        self.rank_list = []
        for i in range(len(self.students) - 1):
            student1, student2 = self.students[i], self.students[i + 1]
            comparison = ""
           
            if all(student1[1][j] > student2[1][j] for j in range(len(student1[0]))):
                comparison += f"{student1[0]} > "
            elif all(student1[1][j] < student2[1][j] for j in range(len(student1[0]))):
                comparison += f"{student2[0]} > "
            else:
                comparison += "# "
            self.rank_list.append(comparison)
        self.rank_list.append(self.students[-1][0])

s = '''A 12 14 16
B 5 6  7
C 17 20 23
D 2 40 12
E 3 41 13
F 7 8 9
G 4 5 6'''

lines = s.split('\n')
students = [[line.split()[0], list(map(int, line.split()[1:]))] for line in lines]
result = Result(students)

result.compare_and_rank()

print("Rank Comparisons:")
for comparison in result.rank_list:
    print(comparison)
