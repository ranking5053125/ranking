def make_ranklist() -> str:
    names_marks = {}
    rank_list = {}
    comparisons = []

    for line in open(r"\\wsl.localhost\Ubuntu\home\rainingrina\problems\pattern\studentRanking.txt"):
        data = line.split()
        name, marks = data[0], list(map(int, data[1:]))
        names_marks[name] = marks

    def all_greater(marks1, marks2):
        return all(m1 > m2 for m1, m2 in zip(marks1, marks2))

    def any_greater(marks1, marks2):
        return any(m1 > m2 for m1, m2 in zip(marks1, marks2))

    sorted_names = sorted(names_marks.keys(), key=lambda name: names_marks[name], reverse=True)

    rank = 1
    while sorted_names:
        current_names = sorted_names[:]
        for i in range(len(current_names)):
            higher = True
            for j in range(len(sorted_names)):
                if current_names[i] != sorted_names[j]:
                    if all_greater(names_marks[sorted_names[j]], names_marks[current_names[i]]):
                        higher = False
                        break
                    elif any_greater(names_marks[current_names[i]], names_marks[sorted_names[j]]):
                        comparisons.append(f"#C {current_names[i]}")
                        higher = False
                        break
            if higher:
                rank_list[current_names[i]] = rank
                sorted_names.remove(current_names[i])
                rank += 1
                break
        else:
            for name in sorted_names:
                rank_list[name] = rank
            rank += 1
            sorted_names = []

    rank_order = ' > '.join(sorted(rank_list, key=rank_list.get))
    comparison_str = '\n'.join(comparisons)
    return f"{rank_order}\n{comparison_str}"

    
print(make_ranklist())